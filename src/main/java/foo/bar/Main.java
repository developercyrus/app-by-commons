package foo.bar;

import developer.cyrus.commons.crypto.AES;

public class Main {
	public static void main(String[] args) {
		String plain = "Hello World!";
		String initVector = "This is init vec";
		String key = "This is P@ssw0rd";
		System.out.println(AES.encrypt(initVector, key, plain));
	}
}
